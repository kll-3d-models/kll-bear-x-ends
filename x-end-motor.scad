// PRUSA iteration4
// X end motor
// GNU GPL v3
// Josef Průša <iam@josefprusa.cz> and contributors
// http://www.reprap.org/wiki/Prusa_Mendel
// http://prusamendel.org

use <config.scad>
use <x-end.scad>

module x_end_motor_base()
{
  x_end_plain();
  translate(v=[-15,31,26.5]) cube(size = [17,44,53], center = true);
}

module x_end_motor_holes()
{
  // Position to place
  translate(v=[-1,32,30.25])
  {
    // Belt hole
    translate(v=[-19,-21,-12]) cube(size = [10,25,22]);

    // Motor mounting holes
    translate(v=[0,-15.5,15.5]) rotate(a=[0,-90,0]) rotate(a=[0,0,90]) cylinder(h=30, r=1.55, $fn=60);
    translate(v=[0,-15.5,15.5]) rotate(a=[0,-90,0]) rotate(a=[0,0,90]) cylinder(h=18, r2=1.55, r1=1.7, $fn=60);
    translate(v=[1,-15.5,15.5]) rotate(a=[0,-90,0]) rotate(a=[0,0,90]) cylinder(h=10, r=3.05, $fn=60);

    translate(v=[20,-15,-15.5]) rotate(a=[0,-90,0]) rotate(a=[0,0,90]) cylinder(h=70, r=1.6, $fn=30);
    translate(v=[1,-15,-15.5]) rotate(a=[0,-90,0]) rotate(a=[0,0,90]) cylinder(h=10, r=3.1, $fn=30);
    translate(v=[20,-15.5,-15.5]) rotate(a=[0,-90,0]) rotate(a=[0,0,90]) cylinder(h=70, r=1.6, $fn=30);
    translate(v=[1,-15.5,-15.5]) rotate(a=[0,-90,0]) rotate(a=[0,0,90]) cylinder(h=10, r=3.1, $fn=30);
    translate(v=[20,-16,-15.5]) rotate(a=[0,-90,0]) rotate(a=[0,0,90]) cylinder(h=70, r=1.6, $fn=30);
    translate(v=[1,-16,-15.5]) rotate(a=[0,-90,0]) rotate(a=[0,0,90]) cylinder(h=10, r=3.1, $fn=30);
    translate(v=[20,-16.5,-15.5]) rotate(a=[0,-90,0]) rotate(a=[0,0,90]) cylinder(h=70, r=1.6, $fn=30);
    translate(v=[1,-16.5,-15.5]) rotate(a=[0,-90,0]) rotate(a=[0,0,90]) cylinder(h=10, r=3.1, $fn=30);

    translate(v=[20,16.,-15.0]) rotate(a=[0,-90,0]) rotate(a=[0,0,90]) cylinder(h=70, r=1.6, $fn=30);
    translate(v=[1,16.,-15.0]) rotate(a=[0,-90,0]) rotate(a=[0,0,90]) cylinder(h=10, r=3.1, $fn=30);
    translate(v=[20,15.5,-15.5]) rotate(a=[0,-90,0]) rotate(a=[0,0,90]) cylinder(h=70, r=1.6, $fn=30);
    translate(v=[1,15.5,-15.5]) rotate(a=[0,-90,0]) rotate(a=[0,0,90]) cylinder(h=10, r=3.1, $fn=30);
    translate(v=[20,15,-16]) rotate(a=[0,-90,0]) rotate(a=[0,0,90]) cylinder(h=70, r=1.6, $fn=30);
    translate(v=[1,15,-16]) rotate(a=[0,-90,0]) rotate(a=[0,0,90]) cylinder(h=10, r=3.1, $fn=30);
    translate(v=[20,14.5,-16.5]) rotate(a=[0,-90,0]) rotate(a=[0,0,90]) cylinder(h=70, r=1.6, $fn=30);
    translate(v=[1,14.5,-16.5]) rotate(a=[0,-90,0]) rotate(a=[0,0,90]) cylinder(h=10, r=3.1, $fn=30);

    // Material saving cutout
    translate(v=[-10,12,12]) cube(size = [60,42,42], center = true);
    translate(v=[-25,10.5,-11]) rotate([45,0,0]) cube(size = [60,10,10]);
    translate(v=[-25,21,-11]) rotate([45,0,0]) cube(size = [60,10,10]);

    // Material saving cutout
    translate(v=[-10,41,-30.5]) rotate(a=[45,0,0])  cube(size = [60,42,42], center = true);
    translate([-15,-2,-33]) rotate([-45,0,0]) cylinder(h=25, r=1.7, $fn=30);
    translate([-15,-2,-33]) rotate([-45,0,0]) cylinder(h=28-18, r=3.2, $fn=30);
    translate([-35+2.8,10,-25]) rotate([45,0,0])  cube(size = [20,2,5.6]);
  }
}

// Motor shaft cutout
module x_end_motor_shaft_cutout()
{
  union()
  {
    difference()
    {
      translate(v=[0,32,30]) rotate(a=[0,-90,0]) rotate(a=[0,0,90]) cylinder(h = 70, r=17, $fn=6);
      translate(v=[-10,-17+32,30]) cube(size = [60,2,10], center = true);
      translate(v=[-10,-8+32,-15.5+30]) rotate(a=[60,0,0]) cube(size = [60,2,10], center = true);
      translate(v=[-10,8+32,-15.5+30]) rotate(a=[-60,0,0]) cube(size = [60,2,10], center = true);
    }

    translate(v=[-30,25.2,-11.8 +30]) rotate(a=[0,90,0]) cylinder(h = 30, r=3, $fn=30);
    translate(v=[-30,19.05,30]) rotate(a=[0,90,0]) cylinder(h = 30, r=3.5, $fn=100);
  }
}

// Final part
module x_end_motor()
{
  difference()
  {
    x_end_motor_base();
    x_end_motor_shaft_cutout();
    x_end_motor_holes();

    // waste pocket
    translate([-15,7,6]) rotate([90,0,0]) cylinder(h=3.5, r=5, $fn=30);
    translate([-15,7,51]) rotate([90,0,0]) cylinder(h=3.5, r=5, $fn=30);
    translate([-15,3.5,6]) rotate([90,0,0]) cylinder(h=3, r1=5, r2=4, $fn=30);
    translate([-15,3.5,51]) rotate([90,0,0]) cylinder(h=3, r1=5, r2=4, $fn=30);

    // rod contact window
    translate([-17,3,55]) cube([4,4,10]);
    translate([-17,3,-8]) cube([4,4,10]);
    translate([-30,-30,58]) cube([30,30,10]);

    // version
    translate([-getBackChunkXSize()/2 - getBearingToXAxisSpacing() + getExtrusionWidth(), 10, 2])
    {
      rotate([90,0,270])
      {
        linear_extrude(height = getExtrusionWidth() + 0.1)
        {
          #text(getVersion(),font = "helvetica:style=Bold", size=4, halign="left");
        }
      }
    }
  }

  translate([-15,10,6]) rotate([90,0,0]) cylinder(h=3, r=5, $fn=30);
  translate([-15,10,51]) rotate([90,0,0]) cylinder(h=3, r=5, $fn=30);
}

difference()
{
  union()
  {
    x_end_motor();
    translate([-8,-17,13.0]) rotate([0,0,44.7]) cube([10,1.5,1]);
  }

  //chamfers
  translate([-47,-40,60]) rotate([0,45,0]) cube([20,80,20]);
  translate([-20,-30.5,69]) rotate([0,45,0]) cube([20,20,20]);
  translate([-25,-37,49]) rotate([45,0,0]) cube([30,20,20]);
  translate([-25,23.5,49]) rotate([45,0,0]) cube([30,20,20]);
}
