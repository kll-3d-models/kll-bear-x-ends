//include <x-end.scad>
use <x-end_idler-mount.scad>

%translate([118, -114.5, 0]) mirror([1, 0, 0]) import("BNB_Short_Ears_X-End_Tensioner_B05.stl", convexity=10);

//translate([-15.125, -28, 14.4]) rotate([90, 0, 0]) import("BNB_Short_Ears_X-End_Idler_Mount.stl", convexity=10);

//translate([-15.125, -17, x_end_idler_mount_height()/2 + 14.4]) x_end_idler_mount();

translate([-15.125, -18.75, 28.145]) x_end_idler_mount(); // y=-17
