include <MCAD/nuts_and_bolts.scad>
use <x-end.scad>
use <x-end-idler.scad>
use <x-end-motor.scad>
use <x-end_idler-mount.scad>
use <util.scad>

// Increase if things are tight or decrease if things are loose.
tolerance = 0.1;

// Printing layer height.
layer_height = 0.25;

// Printing extrusion width.
extrusion_width = 0.5;

// The total height of the nut. The original Prusa part is ??.
trapezoidal_nut_total_height = 14;

// The thickness of just the larger head portion of the nut. The original Prusa part is ??.
trapezoidal_nut_head_height = 3.7;

// The diameter of the larger head portion of the nut. The original Prusa part is 25 and another common size is 22.
trapezoidal_nut_head_diameter = 25;

// The diameter of the hole the nut will slide in to. The original Prusa part is ??.
trapezoidal_nut_hole_diameter = 10.25;

// The spacing between mounting holes on the nut. The original Prusa part is 19 and another common size is 16.
trapezoidal_nut_mount_spacing = 19;

// Flat to flat measurement of your square nuts.
square_nut_width = 5.4;

// Thickness measurement of your square nuts.
square_nut_thickness = 2.4;

// Thickness measurement of your lock nuts.
lock_nut_thickness = 4.0;

//labeled combo box for numbers
belt_path = 28.145; // [28.145:Bear, 30.25:Prusa]

// The distance between the linear bearings and the rods on the X axis.
bearing_to_x_axis_spacing = 15;

// The size of the pushfit hole for the 8mm smooth rod. Needs to fit tightly so usually slightly less than 8mm.
pushfit_rod_diameter = 7.9;

version = "GS 0.1.0";

/*
 * Basic parameter getters.
 */

function getTolerance() = tolerance;

function getLayerHeight() = layer_height;

function getExtrusionWidth() = extrusion_width;

function getTrapezoidalNutTotalHeight() = trapezoidal_nut_total_height;

function getTrapezoidalNutHeadHeight() = trapezoidal_nut_head_height;

function getTrapezoidalNutHeadDiameter() = trapezoidal_nut_head_diameter;

function getTrapezoidalNutHoleDiameter() = trapezoidal_nut_hole_diameter;

function getTrapezoidalNutMountSpacing() = trapezoidal_nut_mount_spacing;

function getSquareNutWidth() = square_nut_width + getTolerance();

function getSquareNutThickness() = square_nut_thickness + getTolerance();

function getLockNutThickness() = lock_nut_thickness + getTolerance();

function getBeltPathOffset() = belt_path;

function getBearingToXAxisSpacing() = bearing_to_x_axis_spacing;

function getPushfitRodDiameter() = pushfit_rod_diameter;

function getVersion() = version;

/*
 * Calculations and non-configurable values needed in more than one file.
 */

function getBoltDiameter() = 3 + getTolerance();

function getBoltCapDiameter() = METRIC_BOLT_CAP_DIAMETERS[3] + getTolerance();

function getBoltCapThickness() = 3 + getTolerance();

function getGt2PulleyWidth() = 9;

function getGt2PulleyDiameter() = 18;

//x_end_idler_mount();
x_end_idler();
//x_end_motor();
//x_end_plain();
//chexagon(10, 10, 2);
/*difference()
{
  ccube([10, 20, 50], 2);
  cube([10, 20, 50], true);
}*/