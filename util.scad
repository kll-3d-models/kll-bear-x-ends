include <MCAD/nuts_and_bolts.scad>

use <config.scad>

// apothem of regular polygon with n sides
function apo(r, n) = r / cos( 180 / n );

function chamfer_offset(chamfer) = sqrt(pow(chamfer, 2) / 2);

// chamfer cube
module ccube(size, chamfer, sides=true)
{
  offset = chamfer_offset(chamfer) * 2;

  hull()
  {
    cube([size[0] - offset, size[1] - offset, size[2]], true);

    if (sides)
    {
      cube([size[0] - offset, size[1], size[2] - offset], true);
      cube([size[0], size[1] - offset, size[2] - offset], true);
    }
    else
    {
      cube([size[0], size[1], size[2] - offset], true);
    }
  }
}

// chamfer hexagon
module chexagon(h, ds=0, dp=0, c=1, rotation=0)
{
  offset = chamfer_offset(c) * 2;
  _d = ds == 0 ? dp : apo(ds/2, 8)*2;

  rotate([0, 0, rotation])
  {
    hull()
    {
      cylinder(h=h, d=_d - offset, center=true, $fn=8);
      cylinder(h=h - offset, d=_d, center=true, $fn=8);
    }
  }
}

module nutTrap(size, offset = 0, thickness = 0, rotation = [0, 0, 0], tolerance = 0)
{
  _tolerance = tolerance == 0 ? getTolerance() : tolerance;
  radius = METRIC_NUT_AC_WIDTHS[size]/2+_tolerance;
  height = (thickness == 0 ? METRIC_NUT_THICKNESS[size] : thickness) + _tolerance;
  apothem = radius*sin(60);

  rotate(rotation)
  translate([0, 0, -height/2])
  {
    hull()
    {
      translate([offset, -apothem, 0])
      {
        cube([0.1, 2*apothem, height]);
      }

      cylinder(r=radius, h=height, $fn = 6);
    }
  }
}

nutTrap(3, 0);
