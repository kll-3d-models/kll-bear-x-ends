include <MCAD/nuts_and_bolts.scad>
use <MCAD/teardrop.scad>

use <config.scad>
use <util.scad>

/*
 * Local parameters.
 */

// The height of the body;
body_height = 28;

// The full depth of the uncut symmetrical version of the body.
uncut_body_depth = 36;

// The width of the chamfer on the thin portion of the body.
chamfer = 1.5;

/*
 * Local calculated values.
 */

chop_offset = getGt2PulleyDiameter()/2;
pulley_cut_margin = 2*getExtrusionWidth();

function getXIdlerMountThinWidth() = getGt2PulleyWidth() + 6*getExtrusionWidth();

function getXIdlerMountThickWidth() = getGt2PulleyWidth() + 4*getExtrusionWidth() + 2*getLockNutThickness();

function getXIdlerMountHeight() = body_height;

function getXIdlerMountDepth() = uncut_body_depth/2 + chop_offset;

function getXIdlerMountBoltSpacing() = getXIdlerMountHeight() - METRIC_NUT_AC_WIDTHS[3] - 2*getTolerance(); // 20;

/*
 * Modules.
 */

module x_end_idler_mount()
{
  difference()
  {
    x_end_idler_body_base();
    x_end_idler_body_cuts();
  }
}

module x_end_idler_mount_cut()
{
  width = getXIdlerMountThickWidth() + 2*getExtrusionWidth();
  depth = getXIdlerMountDepth() + 2*getExtrusionWidth();
  height = getXIdlerMountHeight() + 4*getLayerHeight();

  resize([width, depth, height])
  {
    x_end_idler_body_base();
  }
}

module x_end_idler_body_base()
{
  difference()
  {
    union()
    {
      x_end_idler_thin_chunk();
      x_end_idler_thick_chunk();
    }

    x_end_idler_body_chamfers();

    // main body chop
    translate([0,  -25 - getGt2PulleyDiameter()/2, 0])
    {
      cube([50, 50, 50], center=true);
    }

    // back cutout (just because it looks cool)
    translate([0, uncut_body_depth/2 + 4.592/2, 0])
    {
      rotate([0, 90, 0])
      {
        rotate([0, 0, 22.5])
        {
          cylinder(h=getXIdlerMountThinWidth() + 0.1, d=12, center=true, $fn=8);
        }
      }
    }
  }
}

module x_end_idler_thin_chunk()
{
  x = getXIdlerMountThinWidth();
  y = uncut_body_depth;
  z = getXIdlerMountHeight();
  ccube([x, y, z], chamfer);
}

module x_end_idler_thick_chunk()
{
  thing = sqrt(pow(chamfer, 2)/2);
  inner_size_oct = apo(getXIdlerMountHeight(), 8) - thing;
  outer_size_oct = apo(getGt2PulleyDiameter() + 2, 8);

  rotate([0, 90, 0])
  {
    rotate([0, 0, 22.5])
    {
      hull()
      {
        cylinder(h=getXIdlerMountThickWidth(), d=outer_size_oct, center=true, $fn=8);
        cylinder(h=getXIdlerMountThinWidth(), d=inner_size_oct, center=true, $fn=8);
      }
    }

    translate([0, -outer_size_oct/4, 0])
    {
      rotate([0, 0, 22.5])
      {
        hull()
        {
          cylinder(h=getXIdlerMountThickWidth(), d=outer_size_oct, center=true, $fn=8);
          cylinder(h=getXIdlerMountThinWidth(), d=inner_size_oct, center=true, $fn=8);
        }
      }
    }
  }
}

module x_end_idler_body_cuts()
{
  x_end_idler_pulley_mount();
  x_end_idler_pulley_cut();
  x_end_idler_tensioning_mount();
}

module x_end_idler_body_chamfers()
{
  x_end_idler_body_chamfers_top();
  x_end_idler_body_chamfers_bottom();
}

module x_end_idler_body_chamfers_top()
{
  offset = chamfer_offset(chamfer);
  translate([0, -25, getXIdlerMountHeight()/2])
  {
    translate([getXIdlerMountThinWidth()/2 - offset, 0, 0])
    {
      rotate([0, 45, 0])
      {
        cube([50, 50, 50]);
      }
    }

    mirror([1, 0, 0])
    {
      translate([getXIdlerMountThinWidth()/2 - offset, 0, 0])
      {
        rotate([0, 45, 0])
        {
          cube([50, 50, 50]);
        }
      }
    }
  }
}

module x_end_idler_body_chamfers_bottom()
{
  mirror([0, 0, 1])
  {
    x_end_idler_body_chamfers_top();
  }
}

module x_end_idler_pulley_mount()
{
  // bolt hole
  teardrop(getBoltDiameter()/2, getXIdlerMountThickWidth() + 1, 90);

  // bolt head
  bolt_head_offset = getXIdlerMountThickWidth()/2 - getLockNutThickness()/2 + getTolerance()/2;
  translate([bolt_head_offset, 0, 0])
  {
    teardrop(getBoltCapDiameter()/2, getLockNutThickness(), 90);
  }

  // lock nut
  locknut_offset = -getXIdlerMountThickWidth()/2 + getLockNutThickness()/2 + getTolerance()/2;
  translate([locknut_offset, 0, 0])
  {
    rotate([0, 90, 0])
    {
      rotate([0, 0, 30])
      {
        nutTrap(3, thickness = getLockNutThickness());
      }
    }
  }
}

module x_end_idler_pulley_cut()
{
  width = getGt2PulleyWidth() + 2*pulley_cut_margin;
  outer_diameter = apo(getGt2PulleyDiameter() + pulley_cut_margin, 8);
  inner_diameter = 4;
  offset = width/2 - pulley_cut_margin/2;

  rotate([0, 90, 0])
  {
    difference()
    {
      union()
      {
        rotate([0, 0, 22.5])
        {
          cylinder(h=width, d=outer_diameter, center=true, $fn=8);
        }

        square_circumradius = apo(getGt2PulleyDiameter() + pulley_cut_margin, 4)/2;
        translate([0, -square_circumradius, 0])
        {
          rotate([0, 0, 45])
          {
            cylinder(h=width, r=square_circumradius, center=true, $fn=4);
          }
        }
      }

      for(i=[0, 1])
      {
        rotate([i * 180, 0, 0])
        {
          translate([0, 0, offset])
          {
            cylinder(h=pulley_cut_margin, d1=inner_diameter, d2=getGt2PulleyDiameter(), center=true, $fn=32);
          }
        }
      }
    }
  }

  // debug visualization
  *%rotate([0, 90, 0]) cylinder(h=getGt2PulleyWidth(), d=4, center=true, $fn=32);
}

module x_end_idler_tensioning_mount()
{
  bolt_length = 10;
  offset = getXIdlerMountBoltSpacing() / 2;

  for (i=[-1, 1])
  {
    // bolt hole
    translate([0, uncut_body_depth/2 - bolt_length/2, 0])
    {
      rotate([0, 0, 90])
      {
        translate([0, 0, i*offset])
        {
          teardrop(getBoltDiameter()/2, bolt_length, 90);
        }
      }
    }

    // nut trap
    translate([0, uncut_body_depth/2 - 6.5, i*offset])
    {
      trap_offset = METRIC_NUT_AC_WIDTHS[3]/2 + getTolerance();
      nutTrap(3, trap_offset, thickness = 4, rotation = [0, -i*90, 90]);
    }
  }
}

module x_end_idler_mount_test()
{
  block_depth = getXIdlerMountDepth() + 2;
  block_offset = block_depth/2 - chop_offset;

  %difference()
  {
    translate([0, block_offset, 0])
    {
      cube([getXIdlerMountThickWidth()+4, block_depth, getXIdlerMountHeight()+2], center=true);
    }

    x_end_idler_mount_cut();
  }

  x_end_idler_mount();
}

x_end_idler_mount_test();
